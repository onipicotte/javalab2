// Oni Picotte 2036356

public class BikeStore {
    public static void main(String[] args) {
        Bicycle b1 = new Bicycle("Canadian Tire", 1, 10);
        Bicycle b2 = new Bicycle("iBike", 2, 20);
        Bicycle b3 = new Bicycle("Fixi", 3, 40);
        Bicycle b4 = new Bicycle("eVelo", 2, 30);

        Bicycle[] bikes = {b1,b2,b3,b4};

        for (int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i].toString());
        }
    }
}
