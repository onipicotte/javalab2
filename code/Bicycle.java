// Oni Picotte 2036356

public class Bicycle {
    private String manifacturer;
    private int numberGears;
    private int maxSpeed;

    public Bicycle(String manifacturer, int numberGears, int maxSpeed) {
        this.manifacturer = manifacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManifacturer() {
        return manifacturer;
    }
    public int getNumberGears() {
        return numberGears;
    }
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return "The manifacturer is " + this.manifacturer + ", it has " + this.numberGears + " gears and can go up to " + this. maxSpeed + " km/h";
    }
}
